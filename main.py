import mylib

def main():
    mylib.greet()
    mylib.compute()

if __name__ == "__main__":
    main()
